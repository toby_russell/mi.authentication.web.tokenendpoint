﻿using System;
using System.Collections.Generic;

namespace MI.Authentication.Models
{
    public abstract class PermissionParameterType<T>
    {
        public Guid ParameterTypeId { get; set; }
        public string Name { get; set; }
        public string  Description { get; set; }    
        public List<T> ParameterValue { get; set; }

        protected PermissionParameterType()
        {
            
        }

    }
}