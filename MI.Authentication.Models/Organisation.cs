﻿using System;

namespace MI.Authentication.Models
{
    public class Organisation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public Guid InternalId { get; set; }

        private Organisation()
        {
            
        }

        public Organisation(string name, string domain, Guid internalId)
        {
            Name = name;
            Domain = domain;
            InternalId = internalId;
        }
    }
}