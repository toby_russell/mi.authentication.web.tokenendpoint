﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace MI.Authentication.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsEnabled { get; set; }
        public string Email { get; set; }
        public Guid InternalId { get; set; }
        public Organisation Organisation { get; set; }

        public IList<PermissionAssignment> Permissions { get; set; }
        public IList<RoleAssignment> Roles { get; set; }
             

    }
}
