﻿using System;
using System.Collections.Generic;

namespace MI.Authentication.Models
{
    public class RoleAssignment
    {
        public int Id { get; set; }
        public int RoleTemplateId { get; set; } 
        public string Description { get; set; }
        public string Name { get; set; }
        public Guid InternalId { get; set; }

        public IList<PermissionAssignment> PermissionTemplates { get; } = new List<PermissionAssignment>();

        public IDictionary<int, PermissionAssignmentParameter> ParmaterTemplates { get; } = new Dictionary<int, PermissionAssignmentParameter>();

        public RoleAssignment() { }

        public RoleAssignment(string name, string description)
        {
            Name = name;
            Description = description;
        }
        
    }
}
