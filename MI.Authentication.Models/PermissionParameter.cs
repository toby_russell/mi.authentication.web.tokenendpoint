﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MI.Authentication.Models
{
    public abstract class Parameter
    {
        public int Id { get; set; }   
        public Guid ParameterTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        protected Parameter(Guid parameterTypeId, string name, string description)
        {
            ParameterTypeId = parameterTypeId;
            Name = name;
            Description = description;
        }


    }

    public class RoleAssignmentParameter : Parameter
    {
        public RoleAssignmentParameter(Guid parameterTypeId, string name, string description) : base(parameterTypeId, name, description)
        {
        }
    }

    public class RoleTemplateParameter : Parameter
    {
        public RoleTemplateParameter(Guid parameterTypeId, string name, string description) : base(parameterTypeId, name, description)
        {
        }
    }

    public class PermissionTemplateParameter : Parameter
    {
        public PermissionTemplateParameter(Guid parameterTypeId, string name, string description) : base(parameterTypeId, name, description)
        {
        }
    }

    public class PermissionAssignmentParameter : Parameter
    {
        public PermissionAssignmentParameter(Guid parameterTypeId, string name, string description) : base(parameterTypeId, name, description)
        {
        }
    }
}
