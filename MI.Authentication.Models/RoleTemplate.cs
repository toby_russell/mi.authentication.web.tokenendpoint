﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MI.Authentication.Models
{
    public class RoleTemplate
    {
        public int Id { get; set; }
        public Guid InternalId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<PermissionTemplate> PermissionTemplates { get; } = new List<PermissionTemplate>();

        public IDictionary<int, PermissionTemplateParameter> ParamaterTemplates { get; } = new Dictionary<int, PermissionTemplateParameter>();
 
        public RoleTemplate(string roleTemplateName, string description, Guid internalId)
        {
            Name = roleTemplateName;
            Description = description;
            InternalId = internalId;
        }

        //protected RoleTemplate(RoleTemplate roleTemplate) : this(roleTemplate.Name, )
        //{
        //    throw new System.NotImplementedException();
        //}

        //public ApplicationRole CreateRole()
        //{
        //    return new ApplicationRole { };
        //}
    }

    //public class RoleTemplateInstance : RoleTemplate
    //{
    //    public ICollection<RoleTemplatePermission> PermissionTemplateDetails { get; set; }

    //    //public RoleTemplateInstance(RoleTemplate template, IEnumerable<RoleTemplatePermission> instanceDefaults)// : base(template)
    //    //public RoleTemplateInstance(IEnumerable<RoleTemplatePermission> instanceDefaults)
    //    //    => PermissionTemplateDetails = (ICollection<RoleTemplatePermission>) instanceDefaults;
    //}


}
