﻿namespace MI.Authentication.Models
{
    public class RoleTemplatePermission
    {
        public int PermissionTemplateId { get; set; }
        public string RolePermissionName { get; set; }
        public string RolePermissionDefaultValue { get; set; }
        public string RolePermissionDescription { get; set; }
    }
}
