﻿using System;

namespace MI.Authentication.Models
{
    public class RefreshToken
    {
        public int Id { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
        public string Token { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }
    }
}
