﻿using System;

namespace MI.Authentication.Models
{
    public class Site
    {
        public Guid SiteId { get; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}