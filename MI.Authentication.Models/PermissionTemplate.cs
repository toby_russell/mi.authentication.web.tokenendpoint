﻿using System.Collections.Generic;

namespace MI.Authentication.Models
{
    public class PermissionTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ClaimType { get; set; }
        public string Description { get; set; }

        public List<PermissionTemplateParameter> TemplateParameters { get; } = new List<PermissionTemplateParameter>();

        public PermissionTemplate(string name, string claimType, string description)
        {
            Name = name;
            ClaimType = claimType;
            Description = description;
        }

        public PermissionTemplate(string name, string claimType, string description, IEnumerable<PermissionTemplateParameter> templateParameters) : this(name, claimType, description)
        {
            TemplateParameters.AddRange(templateParameters);
        }
    }

    public class PermissionAssignment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ClaimType { get; set; }
        public string Description { get; set; }
        public List<PermissionAssignmentParameter> PermissionParameters { get; } = new List<PermissionAssignmentParameter>();

        public PermissionAssignment(string name, string claimType, string description)
        {
            Name = name;
            ClaimType = claimType;
            Description = description;
        }

        public PermissionAssignment(string name, string claimType, string description, IEnumerable<PermissionAssignmentParameter> permissionParameters) : this(name, claimType, description)
        {
            PermissionParameters.AddRange(permissionParameters);
        }
    }
}
