﻿namespace MI.Authentication.Models
{
    public static class ModelConstants
    {
        //public const string AdminClaim = "admin";
        //public const string UserClaim = "user";
        //public const string ManageUserClaim = "manage_user";
        //public const string ValueReaderClaim = "read_values";
        //public const string AdminRole = "admin";
        //public const string UserRole = "user";
        //public const string ValueReaderRole = "value";

        public const string RoleClaimType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
    }
}
