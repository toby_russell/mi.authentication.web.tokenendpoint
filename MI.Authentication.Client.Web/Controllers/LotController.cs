﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MI.Authentication.Client.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize(Policy="SiteIdPolicy")]
    public class LotController : Controller
    {
        [HttpGet("[action]/{siteId}")]
        public string Get(string siteId)
        {
            return siteId;
        }

        [HttpGet("[action]/{siteId:int}")]
        public string Gett(int siteId, string site)
        {
            return $"{siteId} is id for site {site}";
        }
    }
}