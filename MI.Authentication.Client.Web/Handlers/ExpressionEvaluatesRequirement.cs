﻿using Microsoft.AspNetCore.Authorization;

namespace MI.Authentication.Client.Web.Handlers
{
    public class ExpressionEvaluatesRequirement : IAuthorizationRequirement
    {
        public string RouteParamName { get; }

        public ExpressionEvaluatesRequirement(string routeParamName)
        {
            RouteParamName = routeParamName;
        }
    }
}