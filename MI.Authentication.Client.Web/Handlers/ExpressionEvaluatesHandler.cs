﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flee.PublicTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;

namespace MI.Authentication.Client.Web.Handlers
{
    public class ExpressionEvaluatesHandler : AuthorizationHandler<ExpressionEvaluatesRequirement>
    {
        private readonly IHttpContextAccessor _context;

        public ExpressionEvaluatesHandler(IHttpContextAccessor context)
        {
            _context = context;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ExpressionEvaluatesRequirement requirement)
        {
            var param = string.Empty;
            if (!string.IsNullOrEmpty(_context.HttpContext.GetRouteData().Values[requirement.RouteParamName]?.ToString()))
                param = _context.HttpContext.GetRouteData().Values[requirement.RouteParamName].ToString() ;
            else if(_context.HttpContext.Request.Query[requirement.RouteParamName].Any())
                param = _context.HttpContext.Request.Query[requirement.RouteParamName][0];

            if (!context.User.HasClaim(c => c.Type == requirement.RouteParamName) || string.IsNullOrWhiteSpace(param))
                return Task.CompletedTask;

            var claim = context.User.FindFirst(c => c.Type == requirement.RouteParamName);

            //var success = false;
            //var dict = ClaimJsonToDictionary(claim.Value);
            //foreach (var kvp in dict)
            //    success = EvaluateExpressionWithParams(param, kvp.Value.Split(',').ToList());

            //success = EvaluateExpressionWithParams(param, claim.Value.Split(',').ToList());

            if (EvaluateExpressionWithParams(param, claim.Value.Split(',').ToList()))
                context.Succeed(requirement);

            return Task.CompletedTask;
        }

        private bool EvaluateExpressionWithParams(string permittedValue, List<string> tokenValues)
        {
            var context = new ExpressionContext();
            context.Imports.AddType(typeof(CustomFunctions));

            var sb = new StringBuilder();
            tokenValues.ForEach(tv => {
                sb.Append($"\"{tv.Trim()}\"");
                if (tokenValues.IndexOf(tv) < tokenValues.Count - 1)
                    sb.Append(",");
            });

            var expression = $"ValidatePermissionToken(\"{ permittedValue }\", { sb })";

            var e = context.CompileDynamic(expression);
            return (bool)e.Evaluate();
            
        }

        private static Dictionary<string, string> ClaimJsonToDictionary(string claim)
            => JsonConvert.DeserializeObject<Dictionary<string, string>>(claim);

        public static class CustomFunctions
        {
            public static bool ValidatePermissionToken(string requiredPermission, params string[] args)
                => args.Any(param => requiredPermission.Equals(param.Trim()));
        }
    }
}