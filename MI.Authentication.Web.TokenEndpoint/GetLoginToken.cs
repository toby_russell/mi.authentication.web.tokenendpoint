﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using MI.Authentication.Models;
using MI.Authentication.Util;
using MI.Framework.Persistence;

namespace MI.Authentication.Web.TokenEndpoint
{
    public class GetLoginToken
    {
        public static TokenProviderOptions GetOptions()
        {
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.Config.GetSection("TokenAuthentication:SecretKey").Value));

            return new TokenProviderOptions
            {
                Path = Configuration.Config.GetSection("TokenAuthentication:TokenPath").Value,
                Audience = Configuration.Config.GetSection("TokenAuthentication:Audience").Value,
                Issuer = Configuration.Config.GetSection("TokenAuthentication:Issuer").Value,
                Expiration = TimeSpan.FromMinutes(Convert.ToInt32(Configuration.Config.GetSection("TokenAuthentication:ExpirationMinutes").Value)),
                RefreshExpiration = TimeSpan.FromMinutes(Convert.ToInt32(Configuration.Config.GetSection("TokenAuthentication:RefreshExpirationMinutes").Value)),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
            };
        }

        public static LoginResponseData Execute(User user, PersistenceScope db, RefreshToken refreshToken = null)
        {
            var options = GetOptions();
            var now = DateTime.UtcNow;

            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.InternalId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim("siteId", "yandi"),
            };

            var userClaims = user.Permissions; //db.Query<ApplicationUser>().Where(i => i.UserId == user.Id);
            foreach (var userClaim in userClaims)
            {
                claims.Add(new Claim(userClaim.ClaimType, userClaim.PermissionParameters[userClaim.Id].Name));
            }

            var userRoles = user.Roles;//db.Query<ApplicationRole>().Where(i => i.Id == user.Id);
            foreach (var userRole in userRoles)
            {
                var role = db.Query<RoleAssignment>().Single(i => i.Id == userRole.Id);
                claims.Add(new Claim(ModelConstants.RoleClaimType, role.Name));
            }

            //if (refreshToken == null)
            //{
            refreshToken = new RefreshToken
            {
                UserId = user.Id.ToString(),
                Token = Guid.NewGuid().ToString("N"),
                IssuedUtc = now,
                ExpiresUtc = now.Add(options.RefreshExpiration),
            };
            //db.(refreshToken);
            //}

            db.Persist(refreshToken);

            var jwt = new JwtSecurityToken(
                issuer: options.Issuer,
                audience: options.Audience,
                claims: claims.ToArray(),
                notBefore: now,
                expires: now.Add(options.Expiration),
                signingCredentials: options.SigningCredentials);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new LoginResponseData
            {
                access_token = encodedJwt,
                refresh_token = refreshToken.Token,
                expires_in = (int)options.Expiration.TotalSeconds,
                userName = user.Email,
                firstName = user.FirstName,
                lastName = user.LastName,
                isAdmin = claims.Any(i => i.Type == ModelConstants.RoleClaimType && i.Value == "admin")
            };
            return response;
        }
    }
}
