﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MI.Authentication.Web.TokenEndpoint.Controllers
{

    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly string[] _vals = { "value1", "value2" };

        // GET api/values
        [Authorize(Roles = "admin")]
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return _vals;
        }

        // GET api/values/5
        [Authorize(Policy = "ValueReader")]
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return $"value: { (_vals.Length > id ? _vals[id] : "No val for id {id}") }";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
