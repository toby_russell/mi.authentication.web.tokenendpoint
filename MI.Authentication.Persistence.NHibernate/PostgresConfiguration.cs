﻿using System;
using System.Linq;
using System.Reflection;
using MI.Framework;
using MI.Framework.Persistence.NHibernate;
using MI.Framework.Validation;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Tool.hbm2ddl;
using Npgsql;

namespace MI.Authentication.Persistence.NHibernate
{
    public class PostgresConfiguration : NHibernateConfiguration
    {
        public class ConnectionString
        {
            // these are named to match the specifications at www.npgsql.org/doc/connection-string-parameters.html
            // note that in older documentation these are not part of the connection string, but ConnectionStringBuilder seems to be fine regardless
            public string Host { get; set; }
            public int Port { get; set; }
            public string Database { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public bool Pooling { get; set; }
            public int MinPoolSize { get; set; }
            public int MaxPoolSize { get; set; }
            public string ClientEncoding { get; set; }
            public int ReadBufferSize { get; set; }
            public int WriteBufferSize { get; set; }

            public override string ToString()
            {
                var builder = new NpgsqlConnectionStringBuilder
                {
                    Host = Host,
                    Port = Port,
                    Database = Database,
                    Username = Username,
                    Password = Password,
                    Pooling = Pooling,
                    MinPoolSize = MinPoolSize,
                    MaxPoolSize = MaxPoolSize,
                    ClientEncoding = ClientEncoding,
                    ReadBufferSize = ReadBufferSize,
                    WriteBufferSize = WriteBufferSize
                };

                return builder.ConnectionString;
            }
        }

        private ConnectionString _connectionString;

        private ModelMapper _mapper = new ModelMapper();

        public PostgresConfiguration(ConnectionString connectionString, bool logSqlInConsole)
        {
            _connectionString = ArgumentValidation.AssertNotNull(connectionString, nameof(connectionString));
            this.DataBaseIntegration(db =>
            {
                db.ConnectionProvider<DriverConnectionProvider>();
                db.Dialect<PostgreSQLDialect>();
                db.Driver<NpgsqlDriver>();
                db.ConnectionString = _connectionString.ToString();
                db.LogSqlInConsole = logSqlInConsole;
            });
        }

        public void AddMappingAssembly(Assembly mappingAssembly)
        {
            var mappingTypes = new[]
            {
                typeof(ClassMapping<>), typeof(SubclassMapping<>), typeof(UnionSubclassMapping<>),
                typeof(JoinedSubclassMapping<>)
            };
            _mapper.AddMappings(mappingAssembly.GetExportedTypes()
                .Where(type => !type.IsAbstract)
                .Where(type => typeof(IConformistHoldersProvider).IsAssignableFrom(type) && !type.IsGenericTypeDefinition)
                .OrderBy(type => GetFirstBaseTypeWithInterface(type, typeof(IConformistHoldersProvider)).GetGenericArguments()[0].GetInheritanceDepth()));
        }

        private Type GetFirstBaseTypeWithInterface(Type type, Type interfaceType)
        {
            while (type.BaseType != null)
            {
                type = type.BaseType;
                if (interfaceType.IsAssignableFrom(type) && !interfaceType.IsAssignableFrom(type.BaseType))
                    return type;
            }

            return null;
        }

        public void CompileMappings()
        {
            AddDeserializedMapping(_mapper.CompileMappingForAllExplicitlyAddedEntities(), Assembly.GetExecutingAssembly().FullName);
        }

        public void CreateDatabase(bool logInSqlConsole)
        {
            AddDeclaredIndices();
            new SchemaExport(this).Create(logInSqlConsole, true);
        }
    }
}