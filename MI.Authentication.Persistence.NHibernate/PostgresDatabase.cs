﻿using System.Reflection;
using MI.Framework.Persistence;
using MI.Framework.Persistence.NHibernate;
using Environment = NHibernate.Cfg.Environment;

namespace MI.Authentication.Persistence.NHibernate
{
    public class PostgresDatabase
    {
        public static void Connect(string hostServer, int port, string dbName, string user, string password, bool logSqlInConsole)
        {
            PostgresConfiguration.ConnectionString connectionString = new PostgresConfiguration.ConnectionString()
            {
                Host = hostServer,
                Port = port,
                Database = dbName,
                Username = user,
                Password = password,
                ClientEncoding = "UTF8"
            };

            PostgresConfiguration cfg = new PostgresConfiguration(connectionString, logSqlInConsole);
            //cfg.AddMappingAssembly(typeof( ??? ).Assembly); // ??? was BrowserNodeMapping in Modeller
            cfg.AddMappingAssembly(Assembly.GetExecutingAssembly());
            cfg.CompileMappings();
            //cfg.CreateDatabase(logSqlInConsole);

            Environment.Properties[Environment.MaxFetchDepth] = "3";

            PersistenceContext.SetProvider(new NHibernateProvider());
            NHibernateProvider.Configure(cfg);
        }
    }

}
