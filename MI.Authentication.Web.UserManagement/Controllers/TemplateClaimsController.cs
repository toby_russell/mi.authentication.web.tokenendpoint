﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MI.Authentication.Models;

namespace MI.Authentication.Web.UserManagement.Controllers
{
    public class TemplateClaimsController : Controller
    {
        private readonly ApiDbContext _context;

        public TemplateClaimsController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: TemplateClaims
        public async Task<IActionResult> Index()
        {
            return View(await _context.PermissionTemplates.ToListAsync());
        }

        // GET: TemplateClaims/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var templateClaim = await _context.PermissionTemplates.Include("ClaimsTemplateTemplateClaims.ClaimsTemplate")
                .SingleOrDefaultAsync(m => m.Id == id);
            if (templateClaim == null)
            {
                return NotFound();
            }

            return View(templateClaim);
        }

        // GET: TemplateClaims/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TemplateClaims/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,DefaultValue,ClaimType")] PermissionTemplate templateClaim)
        {
            if (ModelState.IsValid)
            {
                _context.Add(templateClaim);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(templateClaim);
        }

        // GET: TemplateClaims/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var templateClaim = await _context.PermissionTemplates.SingleOrDefaultAsync(m => m.Id == id);
            if (templateClaim == null)
            {
                return NotFound();
            }
            return View(templateClaim);
        }

        // POST: TemplateClaims/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,DefaultValue,ClaimType")] PermissionTemplate templateClaim)
        {
            if (id != templateClaim.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(templateClaim);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TemplateClaimExists(templateClaim.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(templateClaim);
        }

        // GET: TemplateClaims/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var templateClaim = await _context.PermissionTemplates
                .SingleOrDefaultAsync(m => m.Id == id);
            if (templateClaim == null)
            {
                return NotFound();
            }

            return View(templateClaim);
        }

        // POST: TemplateClaims/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var templateClaim = await _context.PermissionTemplates.SingleOrDefaultAsync(m => m.Id == id);
            _context.PermissionTemplates.Remove(templateClaim);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TemplateClaimExists(int id)
        {
            return _context.PermissionTemplates.Any(e => e.Id == id);
        }
    }
}
