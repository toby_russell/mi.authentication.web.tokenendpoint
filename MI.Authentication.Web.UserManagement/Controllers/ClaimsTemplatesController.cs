﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.Rendering;
//using Microsoft.EntityFrameworkCore;
//using MI.Authentication.Models;

//namespace MI.Authentication.Web.UserManagement.Controllers
//{
//    public class ClaimsTemplatesController : Controller
//    {
//        private readonly ApiDbContext _context;

//        public ClaimsTemplatesController(ApiDbContext context)
//        {
//            _context = context;
//        }

//        // GET: ClaimsTemplates
//        public async Task<IActionResult> Index()
//        {
//            return View(await _context.ClaimsTemplates.ToListAsync());
//        }

//        // GET: ClaimsTemplates/Details/5
//        public async Task<IActionResult> Details(int? id)
//        {
//            if (id == null)
//            {
//                return NotFound();
//            }

//            var claimsTemplate = await _context.ClaimsTemplates
//                .Include("ClaimsTemplateTemplateClaims.TemplateClaim")
//                .SingleOrDefaultAsync(m => m.Id == id);



//            if (claimsTemplate == null)
//            {
//                return NotFound();
//            }

//            return View(claimsTemplate);
//        }

//        // GET: ClaimsTemplates/Create
//        public IActionResult Create()
//        {
//            return View();
//        }

//        // POST: ClaimsTemplates/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public async Task<IActionResult> Create([Bind("Id,Name")] ClaimsTemplate claimsTemplate)
//        {
//            if (ModelState.IsValid)
//            {
//                _context.Add(claimsTemplate);
//                await _context.SaveChangesAsync();
//                return RedirectToAction(nameof(Index));
//            }
//            return View(claimsTemplate);
//        }

//        // GET: ClaimsTemplates/Edit/5
//        public async Task<IActionResult> Edit(int? id)
//        {
//            if (id == null)
//            {
//                return NotFound();
//            }

//            var claimsTemplate = await _context.ClaimsTemplates.SingleOrDefaultAsync(m => m.Id == id);
//            if (claimsTemplate == null)
//            {
//                return NotFound();
//            }
//            return View(claimsTemplate);
//        }

//        // POST: ClaimsTemplates/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] ClaimsTemplate claimsTemplate)
//        {
//            if (id != claimsTemplate.Id)
//            {
//                return NotFound();
//            }

//            if (ModelState.IsValid)
//            {
//                try
//                {
//                    _context.Update(claimsTemplate);
//                    await _context.SaveChangesAsync();
//                }
//                catch (DbUpdateConcurrencyException)
//                {
//                    if (!ClaimsTemplateExists(claimsTemplate.Id))
//                    {
//                        return NotFound();
//                    }
//                    else
//                    {
//                        throw;
//                    }
//                }
//                return RedirectToAction(nameof(Index));
//            }
//            return View(claimsTemplate);
//        }

//        // GET: ClaimsTemplates/Delete/5
//        public async Task<IActionResult> Delete(int? id)
//        {
//            if (id == null)
//            {
//                return NotFound();
//            }

//            var claimsTemplate = await _context.ClaimsTemplates
//                .SingleOrDefaultAsync(m => m.Id == id);
//            if (claimsTemplate == null)
//            {
//                return NotFound();
//            }

//            return View(claimsTemplate);
//        }

//        // POST: ClaimsTemplates/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public async Task<IActionResult> DeleteConfirmed(int id)
//        {
//            var claimsTemplate = await _context.ClaimsTemplates.SingleOrDefaultAsync(m => m.Id == id);
//            _context.ClaimsTemplates.Remove(claimsTemplate);
//            await _context.SaveChangesAsync();
//            return RedirectToAction(nameof(Index));
//        }

//        private bool ClaimsTemplateExists(int id)
//        {
//            return _context.ClaimsTemplates.Any(e => e.Id == id);
//        }
//    }
//}
