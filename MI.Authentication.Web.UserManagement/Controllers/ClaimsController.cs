﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MI.Authentication.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MI.Authentication.Web.UserManagement.Controllers
{
    public class ClaimsController : Controller
    {
        private readonly ApiDbContext _ctx;

        public ClaimsController(ApiDbContext ctx)
        {
            _ctx = ctx;
        }


        [Authorize]
        public IActionResult Index()
        {
            return View(User?.Claims);
        }

        //[Authorize]
        //public IActionResult CreateClaim()
        //{
        //    _ctx.Add(new Claim())
        //    return null;
        //}
    }
}
