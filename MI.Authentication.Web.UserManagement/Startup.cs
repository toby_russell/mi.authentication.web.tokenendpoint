﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MI.Authentication.Models;
using MI.Authentication.Web.UserManagement.Utilities;

namespace MI.Authentication.Web.UserManagement
{
    public class Startup
    {
        private readonly IConfigurationRoot _configuration;


        public Startup(IHostingEnvironment env)
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .Build();


        }

        public void ConfigureServices(IServiceCollection services)
        {

            // Ser up EF Core
            var connection = _configuration["DefaultConnection"];
            services.AddDbContext<ApiDbContext>(options => options.UseSqlServer(connection));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApiDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.Cookie.Expiration = TimeSpan.FromDays(150);
                options.LoginPath = "/Account/Login"; // If the LoginPath is not set here, ASP.NET Core will default to /Account/Login
                options.LogoutPath = "/Account/Logout"; // If the LogoutPath is not set here, ASP.NET Core will default to /Account/Logout
                options.AccessDeniedPath = "/Account/AccessDenied"; // If the AccessDeniedPath is not set here, ASP.NET Core will default to /Account/AccessDenied
                options.SlidingExpiration = true;
            });

            // Register a custom password validator
            // Have to add after AddIdentity Service. Otherwise the built-in password validation won't work
            services.AddTransient<IPasswordValidator<ApplicationUser>, NoNamePasswordValidator>();

            // Register a custom user validator
            services.AddTransient<IUserValidator<ApplicationUser>, EmailUserValidator>();

            // Register a custom policy
            services.AddTransient<IAuthorizationHandler, BlockCountriesHandler>();

            // Auth 2/2: Use Cookies (step 1) to create the default authorization policy
            //services.AddAuthorization(opts =>
            //{
            //    opts.DefaultPolicy = new AuthorizationPolicyBuilder("Cookie")
            //        .RequireAuthenticatedUser().Build();

//                opts.AddPolicy("NewZealandCustomers", policy =>
//                {
//                    policy.RequireRole("Customers");
//                    policy.RequireClaim(ClaimTypes.Country, "New Zealand");
//                });

//                opts.AddPolicy("NotNewZealand", policy =>
//                {
//                    policy.RequireAuthenticatedUser();
//                    policy.AddRequirements(new BlockCountriesRequirement(new string[]
//                    {
//                        "new Zealand"
//                    }));
//                });

//                opts.AddPolicy("Schedule", policy =>
//                {
//                    policy.AddRequirements(new ScheduleAuthorizationRequirement
//                    {
//                        AllowManager = true,
//                        AllowAssistant = true
//                    })
//;
//                });
//          });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();

                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            // Add Identity which allows user info to be excluded from the requests and responses
            app.UseAuthentication();

            // Auth 1/2: Allow cookie authentication
            //app.UseCookieAuthentication(new CookieAuthenticationOptions
            //{
            //    AuthenticationScheme = "Cookie",
            //    AutomaticAuthenticate = true,
            //    AutomaticChallenge = true,
            //    LoginPath = new PathString("/UserAccount/Login"),
            //    AccessDeniedPath = new PathString("/UserAccount/AccessDenied")
            //});

            //app.UseClaimsTransformation(ClaimsProvider.AddClaims);

            //app.UseMvcWithDefaultRoute();

            //UserDbContextSeed.Seed(app.ApplicationServices).Wait();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
