﻿using Microsoft.AspNetCore.Identity;

namespace MI.Authentication.Web.UserManagement.Utilities
{
    public class CustomIdentityErrorDescriber : IdentityErrorDescriber
    {
        public override IdentityError InvalidUserName(string userName)
        {
            return new IdentityError
            {
                Code = nameof(InvalidUserName),
                Description = "User name can only contain letters."
            };
        }
    }
} 