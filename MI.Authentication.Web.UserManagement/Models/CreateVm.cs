﻿using System.ComponentModel.DataAnnotations;

namespace MI.Authentication.Web.UserManagement.Models
{
    public class CreateVm
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
