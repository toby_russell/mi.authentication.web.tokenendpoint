﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using MI.Authentication.Models;

namespace MI.Authentication.Web.UserManagement.Models
{
    public class EditRoleVm
    {
        public IdentityRole Role { get; set; }
        public IEnumerable<ApplicationUser> Members { get; set; }
        public IEnumerable<ApplicationUser> NonMembers { get; set; }
    }
}
