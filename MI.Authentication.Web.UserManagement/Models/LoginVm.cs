﻿using System.ComponentModel.DataAnnotations;

namespace MI.Authentication.Web.UserManagement.Models
{
    public class LoginVm
    {
        //[Required]
        //[EmailAddress]
        //[UIHint("email")]
        //public string Email { get; set; }

        [Required]
        //[EmailAddress]
        //[UIHint("email")]
        public string Username { get; set; }

        [Required]
        [UIHint("password")]
        public string Password { get; set; }
    }
}
