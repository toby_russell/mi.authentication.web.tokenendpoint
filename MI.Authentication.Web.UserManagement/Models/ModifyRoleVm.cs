﻿using System.ComponentModel.DataAnnotations;

namespace MI.Authentication.Web.UserManagement.Models
{
    public class ModifyRoleVm
    {
        [Required]
        public string RoleName { get; set; }

        public string RoleId { get; set; }
        public string[] IdsToAdd { get; set; }
        public string[] IdsToRemove { get; set; }

    }
}
