﻿using System;
using MI.Authentication.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Authentication.Model.Persistence
{
    public class OrganisationMapping : ClassMapping<Organisation>
    {
        
        public OrganisationMapping()
        {
            Lazy(false);

            Id(o => o.Id, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            Property(o => o.Domain, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.InternalId, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.Name, property => { property.NotNullable(true); property.Access(Accessor.Property); });

        }
    }

    public class UserMapping : ClassMapping<User>
    {
        public UserMapping()
        {
            Id(o => o.Id, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            Property(o => o.FirstName, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.LastName, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.InternalId, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.Email, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.IsEnabled, property => { property.NotNullable(true); property.Access(Accessor.Property); });

            ManyToOne(o => o.Organisation, manyToOne =>
            {
                manyToOne.Column("Id");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(true);
                manyToOne.Cascade(Cascade.None);
            });


        }
    }

    public class RefeshTokenMapping : ClassMapping<RefreshToken>
    {
        public RefeshTokenMapping()
        {
            Id(o => o.Id, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });
        }


    }
}
