﻿using System.ComponentModel.DataAnnotations;

namespace MI.Authentication.UserManagement.Web.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
