﻿using System.Threading.Tasks;

namespace MI.Authentication.UserManagement.Web.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
