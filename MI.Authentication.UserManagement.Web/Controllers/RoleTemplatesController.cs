﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MI.Authentication.Models;

namespace MI.Authentication.UserManagement.Web.Controllers
{
    
    public class RoleTemplatesController : Controller
    {
        private readonly ApiDbContext _context;

        public RoleTemplatesController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: RoleTemplates
        public async Task<IActionResult> Index()
        {
            var model = await _context.RoleTemplates.Include("RoleTemplatePermissionTemplate.PermissionTemplate")
                .ToListAsync();
            return View(model);
        }

        // GET: RoleTemplates/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roleTemplate = await _context.RoleTemplates
                .Include("RoleTemplatePermissionTemplate.PermissionTemplate")
                .SingleOrDefaultAsync(m => m.Id == id);
            if (roleTemplate == null)
            {
                return NotFound();
            }

            return View(roleTemplate);
        }

        // GET: RoleTemplates/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RoleTemplates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] RoleTemplate roleTemplate)
        {
            if (ModelState.IsValid)
            {
                _context.Add(roleTemplate);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(roleTemplate);
        }

        // GET: RoleTemplates/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roleTemplate = await _context.RoleTemplates.Include("RoleTemplatePermissionTemplate.PermissionTemplate").SingleOrDefaultAsync(m => m.Id == id);
            if (roleTemplate == null)
            {
                return NotFound();
            }
            return View(roleTemplate);
        }

        // POST: RoleTemplates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] RoleTemplate roleTemplate)
        {
            if (id != roleTemplate.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(roleTemplate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoleTemplateExists(roleTemplate.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(roleTemplate);
        }

        // GET: RoleTemplates/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roleTemplate = await _context.RoleTemplates
                .SingleOrDefaultAsync(m => m.Id == id);
            if (roleTemplate == null)
            {
                return NotFound();
            }

            return View(roleTemplate);
        }

        // POST: RoleTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var roleTemplate = await _context.RoleTemplates.SingleOrDefaultAsync(m => m.Id == id);
            _context.RoleTemplates.Remove(roleTemplate);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RoleTemplateExists(int id)
        {
            return _context.RoleTemplates.Any(e => e.Id == id);
        }
    }
}
